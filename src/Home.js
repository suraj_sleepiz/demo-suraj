import * as React from 'react';
import { View, Text ,PermissionsAndroid} from 'react-native';
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import BackgroundService from 'react-native-background-actions';
import BackgroundJob from 'react-native-background-actions';
import { openDatabase } from 'react-native-sqlite-storage';
var RNFS = require('react-native-fs');
const sleep = time => new Promise(resolve => setTimeout(() => resolve(), time));
var db = openDatabase({ name: 'UserDatabase.db', createFromLocation : `${RNFS.ExternalStorageDirectoryPath}/MyApp/UserDatabase.db` });

export default class HomeScreen extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            dateAry: []
        }
        this.Options = {
          taskName: 'Demo',
          taskTitle: 'Demo Running',
          taskDesc: 'Demo',
          taskIcon: {
              name: 'ic_launcher',
              type: 'mipmap',
          },
          color: '#ff00ff',
          parameters: {
              delay: 5000,
          },
          actions: '["Exit"]'
      };
    }
    
createTbl() {
  db.transaction(function (txn) {
    txn.executeSql(
      "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
      [],
      function (tx, res) {
        console.log('item:', res.rows.length);
        if (res.rows.length == 0) {
          txn.executeSql('DROP TABLE IF EXISTS table_user', []);
          txn.executeSql(
            'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
            []
          );
        }
      }
    );
  });
}
    async VeryIntensiveTask(taskDataArguments)
    {
        const { delay } = taskDataArguments;
        await new Promise(async (resolve) => {
            var i = 0;
            for (let i = 0; BackgroundJob.isRunning(); i++) {  message: "Success DOOD "+i
                // })
               await sleep(delay);    
              }                     
        });
    }

   



    componentDidMount() {
        this.getDataFromServer()
        this.saveData()
        this.Start()
        this.createTbl()
    }
    Start()
    {
        BackgroundService.start(this.VeryIntensiveTask, this.Options);
    }
Stop()
    {
        BackgroundService.stop();
    }

    async getDataFromServer () {
    try {
      const response = await fetch(
        'https://date.nager.at/api/v2/publicholidays/2020/US'
      );
      const json = await response.json();
      this.setState({dateAry: json})
      
      return json.movies;
    } catch (error) {
      console.error(error);
    }
    }


     saveData = async () => {
      
      try {
        const granted = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        ]);
      } catch (err) {
        console.warn(err);
      }
      const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
      const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
      if(!readGranted || !writeGranted) {
        console.log('Read and write permissions have not been granted');
        return;
      }
      var path = `${RNFS.ExternalStorageDirectoryPath}/MyApp`;
      RNFS.mkdir(path);
      path += '/UserDatabase.db';
      alert(path)
      RNFS.writeFile(path, db, 'utf8')
        .then((success) => {
          console.log('Success');
        })
        .catch((err) => {
          console.log(err.message);
        });
    }
    render() {
        const {dateAry} = this.state
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Card>
        <Card.Title>US Holiday list</Card.Title>
        <Card.Divider/>
        {
    dateAry && dateAry.map((u, i) => {
      return (
        <View key={i} >
          
          <Text style={{color:'black'}}>{u.localName}</Text>
        </View>
      );
    })
  }
        </Card>
      </View>
    )
    }
  }